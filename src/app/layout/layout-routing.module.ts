import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MainLayoutComponent} from './main-layout/main-layout.component';

const routes: Routes = [
  {
    path: 'auth',
    loadChildren: () => import('../auth/auth.module').then(m => m.AuthModule),
  },
  {
    path: 'operarios',
    component: MainLayoutComponent,
    loadChildren: () => import('../operarios/operarios.module').then(m => m.OperariosModule),
  },
  {
    path: 'remiseros',
    component: MainLayoutComponent,
    loadChildren: () => import('../remiseros/remiseros.module').then(m => m.RemiserosModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule {}
