import {Component, OnInit} from '@angular/core';
import {Remisero} from '../../models/remiseros.interface';
import {RemiserosService} from '../../services/remiseros.service';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.scss']
})
export class ListadoComponent implements OnInit {

  remiseros!: Remisero[];

  constructor(private remiseroService: RemiserosService) {}

  ngOnInit(): void {
    this.remiseroService.getRemiseros()
      .subscribe((resp: Remisero[]) => this.remiseros = resp)
  }

}
