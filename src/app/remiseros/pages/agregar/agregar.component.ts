import {Component, OnInit} from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ActivatedRoute, Router} from '@angular/router';
import {switchMap, tap} from 'rxjs/operators';
import {Remisero} from '../../models/remiseros.interface';
import {RemiserosService} from '../../services/remiseros.service';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.component.html',
  styleUrls: ['./agregar.component.scss']
})
export class AgregarComponent implements OnInit {

  prestado = [
    {
      desc: 'Si',
      value: true
    },
    {
      desc: 'No',
      value: false
    }
  ]

  estados = [
    {
      desc: 'Activo',
      value: true
    },
    {
      desc: 'Inactivo',
      value: false
    }
  ]

  remisero: Remisero = {
    nombre: '',
    apellido: '',
    dni: '',
    estado: true,
    vehiculo: 0,
    prestado: true
  }

  fecha: Date = new Date();


  constructor(private remiseroService: RemiserosService,
    private activatedRouter: ActivatedRoute,
    private router: Router,
    private snackBar: MatSnackBar) {}

  ngOnInit(): void {

    if (!this.router.url.includes('editar')) {
      return;
    }
    this.activatedRouter.params.pipe(
      switchMap(({id}) => this.remiseroService.getRemiseroById(id)),
    ).subscribe(remisero => this.remisero = remisero)

  }

  guardar() {
    console.log(this.remisero)

    if (!this.isValidForm()) {
      return;
    }
    console.log(this.remisero)

    if (this.remisero.id) {
      this.remiseroService.actualizarRemisero(this.remisero)
        .subscribe(remisero => {
          this.router.navigate(['/remiseros/listado'])
          this.mostrarSnackBar('Registro actualizado')
        })
    } else {
      this.remiseroService.agregarRemisero(this.remisero)
        .subscribe(remisero => {
          this.router.navigate(['/remiseros/editar/', remisero.id])
          this.mostrarSnackBar('Registro creado')
        })
    }

  }

  isValidForm() {
    if (this.remisero.apellido.trim().length === 0) {
      return false;
    }

    if (this.remisero.nombre.trim().length === 0) {
      return false;
    }

    if (this.remisero.dni.trim().length === 0) {
      return false;
    }

    if (this.remisero.vehiculo === 0) {
      return false;
    }
    return true;
  }

  mostrarSnackBar(mensaje: string) {
    this.snackBar.open(mensaje, 'ok!', {
      duration: 2500
    })
  }

}
