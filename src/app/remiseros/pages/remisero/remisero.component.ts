import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Remisero} from '../../models/remiseros.interface';
import {RemiserosService} from '../../services/remiseros.service';
import {switchMap} from 'rxjs/operators';

@Component({
  selector: 'app-remisero',
  templateUrl: './remisero.component.html',
  styleUrls: ['./remisero.component.scss']
})
export class RemiseroComponent implements OnInit {

  remisero!: Remisero;

  constructor(private remiseroService: RemiserosService,
    private activatedRouter: ActivatedRoute,
    private router: Router) {

  }

  ngOnInit(): void {
    this.activatedRouter.params.pipe(
      switchMap(({id}) => this.remiseroService.getRemiseroById(id))
    ).subscribe(remisero => this.remisero = remisero);
  }

  regresar(): void {
    this.router.navigate(['/remiseros/listado']);
  }


}
