import {Pipe, PipeTransform} from '@angular/core';
import {Remisero} from '../models/remiseros.interface';

@Pipe({
  name: 'image',
  pure: false
})
export class ImagePipe implements PipeTransform {

  transform(remisero: Remisero): unknown {
    if (!remisero.id && !remisero.img) {
      return `assets/no-image.png`;
    }

    if (remisero.img) {
      return remisero.img;
    }

    return `assets/no-image.png`
  }

}
