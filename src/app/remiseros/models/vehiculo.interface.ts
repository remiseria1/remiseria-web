export interface Vehiculo {
  id: number,
  modelo: string,
  marcar: string,
  detalle?: string,
  img?: string

}
