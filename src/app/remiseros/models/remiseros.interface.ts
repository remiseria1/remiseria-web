export interface Remisero {
  id?: number,
  nombre: string,
  apellido: string,
  dni: string,
  estado: boolean,
  vehiculo: number,
  img?: string,
  prestado?: boolean
}
