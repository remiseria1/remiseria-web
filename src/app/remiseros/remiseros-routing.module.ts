import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AgregarComponent} from './pages/agregar/agregar.component';
import {ListadoComponent} from './pages/listado/listado.component';
import {RemiseroComponent} from './pages/remisero/remisero.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'listado',
        component: ListadoComponent
      },
      {
        path: 'agregar',
        component: AgregarComponent
      },
      {
        path: 'editar/:id',
        component: AgregarComponent
      },
      {
        path: ':id',
        component: RemiseroComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RemiserosRoutingModule {}
