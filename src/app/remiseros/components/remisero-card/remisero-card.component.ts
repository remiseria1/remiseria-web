import {Component, Input, OnInit} from '@angular/core';
import {Remisero} from '../../models/remiseros.interface';

@Component({
  selector: 'app-remisero-card',
  templateUrl: './remisero-card.component.html',
  styleUrls: ['./remisero-card.component.scss']
})
export class RemiseroCardComponent implements OnInit {

  @Input() remisero!: Remisero;

  constructor() {}

  ngOnInit(): void {
  }

}
