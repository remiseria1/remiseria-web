import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from 'src/environments/environment';
import {Remisero} from '../models/remiseros.interface';

@Injectable({
  providedIn: 'root'
})
export class RemiserosService {

  private url: string;

  constructor(private http: HttpClient) {
    this.url = environment.baseUrl + '/remiseros';
  }

  getRemiseros(): Observable<Remisero[]> {
    return this.http.get<Remisero[]>(this.url);
  }

  getRemiseroById(id: string): Observable<Remisero> {
    return this.http.get<Remisero>(this.url + `/${id}`);
  }

  agregarRemisero(remisero: Remisero): Observable<Remisero> {
    return this.http.post<Remisero>(this.url, remisero);
  }

  actualizarRemisero(remisero: Remisero): Observable<Remisero> {
    return this.http.put<Remisero>(`${this.url}/${remisero.id}`, remisero);
  }
}
