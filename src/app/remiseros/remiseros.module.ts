import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {RemiserosRoutingModule} from './remiseros-routing.module';
import {AgregarComponent} from './pages/agregar/agregar.component';
import {ListadoComponent} from './pages/listado/listado.component';
import {RemiseroComponent} from './pages/remisero/remisero.component';
import {RemiseroCardComponent} from './components/remisero-card/remisero-card.component';
import {ImagePipe} from './pipes/image.pipe';
import {MaterialModule} from '../material/material.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {FormsModule} from '@angular/forms';


@NgModule({
  declarations: [
    AgregarComponent,
    ListadoComponent,
    RemiseroComponent,
    RemiseroCardComponent,
    ImagePipe
  ],
  imports: [
    CommonModule,
    RemiserosRoutingModule,
    MaterialModule,
    FlexLayoutModule,
    FormsModule
  ]
})
export class RemiserosModule {}
