import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from 'src/environments/environment';
import {Operario} from '../models/operario.interface';

@Injectable({
  providedIn: 'root'
})
export class OperariosService {

  private url: string;

  constructor(private http: HttpClient) {
    this.url = environment.baseUrl + '/operarios';
  }

  getOperarios(): Observable<Operario[]> {
    return this.http.get<Operario[]>(this.url);
  }

  getOperarioById(id: string): Observable<Operario> {
    return this.http.get<Operario>(this.url + `/${id}`);
  }

  agregarOperario(operario: Operario): Observable<Operario> {
    return this.http.post<Operario>(this.url, operario);
  }

  actualizarOperario(operario: Operario): Observable<Operario> {
    return this.http.put<Operario>(`${this.url}/${operario.id}`, operario);
  }
}
