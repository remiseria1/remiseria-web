import {Component, Input, OnInit} from '@angular/core';
import {Operario} from '../../models/operario.interface';

@Component({
  selector: 'app-operario-card',
  templateUrl: './operario-card.component.html',
  styleUrls: ['./operario-card.component.scss']
})
export class OperarioCardComponent implements OnInit {

  @Input() operario!: Operario;

  constructor() {}

  ngOnInit(): void {
  }

}
