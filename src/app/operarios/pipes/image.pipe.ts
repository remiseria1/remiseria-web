import {Pipe, PipeTransform} from '@angular/core';
import {Operario} from '../models/operario.interface';

@Pipe({
  name: 'image',
  pure: false
})
export class ImagePipe implements PipeTransform {

  transform(operario: Operario): string {

    if (!operario.id && !operario.img) {
      return `assets/no-image.png`;
    }

    if (operario.img) {
      return operario.img;
    }

    return `assets/no-image.png`

  }

}
