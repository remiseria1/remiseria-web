import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AgregarComponent} from './pages/agregar/agregar.component';
import {BuscarComponent} from './pages/buscar/buscar.component';
import {CrearComponent} from './pages/crear/crear.component';
import {ListadoComponent} from './pages/listado/listado.component';
import {OperarioComponent} from './pages/operario/operario.component';
import {OperariosRoutingModule} from './operarios-routing.module';
import {MaterialModule} from '../material/material.module';
import {OperarioCardComponent} from './components/operario-card/operario-card.component';
import {ImagePipe} from './pipes/image.pipe';
import {FlexLayoutModule} from '@angular/flex-layout';
import {FormsModule} from '@angular/forms';



@NgModule({
  declarations: [
    ListadoComponent,
    CrearComponent,
    AgregarComponent,
    BuscarComponent,
    OperarioComponent,
    OperarioCardComponent,
    ImagePipe,
  ],
  imports: [
    CommonModule,
    OperariosRoutingModule,
    MaterialModule,
    FlexLayoutModule,
    FormsModule,
  ]
})
export class OperariosModule {}
