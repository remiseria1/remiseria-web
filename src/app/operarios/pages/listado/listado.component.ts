import {Component, OnInit} from '@angular/core';
import {Operario} from '../../models/operario.interface';
import {OperariosService} from '../../services/operarios.service';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.scss']
})
export class ListadoComponent implements OnInit {

  operarios!: Operario[];

  constructor(private operariosService: OperariosService) {}

  ngOnInit(): void {
    this.operariosService.getOperarios()
      .subscribe((resp: Operario[]) => this.operarios = resp)
  }

}
