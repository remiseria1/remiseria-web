import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Operario} from '../../models/operario.interface';
import {OperariosService} from '../../services/operarios.service';
import {switchMap} from 'rxjs/operators';

@Component({
  selector: 'app-operario',
  templateUrl: './operario.component.html',
  styleUrls: ['./operario.component.scss']
})
export class OperarioComponent implements OnInit {

  operario!: Operario;

  constructor(private operariosService: OperariosService,
    private activatedRouter: ActivatedRoute,
    private router: Router) {

  }

  ngOnInit(): void {
    this.activatedRouter.params.pipe(
      switchMap(({id}) => this.operariosService.getOperarioById(id))
    ).subscribe(operario => this.operario = operario);
  }

  regresar(): void {
    this.router.navigate(['/operarios/listado']);
  }

}
