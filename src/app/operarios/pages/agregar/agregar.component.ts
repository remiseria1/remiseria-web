import {Component, OnInit} from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ActivatedRoute, Router} from '@angular/router';
import {switchMap, tap} from 'rxjs/operators';
import {Operario} from '../../models/operario.interface';
import {OperariosService} from '../../services/operarios.service';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.component.html',
  styleUrls: ['./agregar.component.scss']
})
export class AgregarComponent implements OnInit {

  turnos = [
    {desc: 'Mañana'},
    {desc: 'Tarde'},
    {desc: 'Noche'}
  ]

  estados = [
    {
      desc: 'Activo',
      value: true
    },
    {
      desc: 'Inactivo',
      value: false
    }
  ]

  operador: Operario = {
    nombre: '',
    apellido: '',
    dni: '',
    estado: true,
    direccion: '',
    fechaNacimiento: '',
    turno: 'Mañana'
  }

  fecha: Date = new Date();


  constructor(private operariosService: OperariosService,
    private activatedRouter: ActivatedRoute,
    private router: Router,
    private snackBar: MatSnackBar) {}

  ngOnInit(): void {

    if (!this.router.url.includes('editar')) {
      return;
    }
    this.activatedRouter.params.pipe(
      switchMap(({id}) => this.operariosService.getOperarioById(id)),
      tap(({fechaNacimiento}) => {
        const date = fechaNacimiento.split('/');
        this.fecha = new Date(Number(date[2]), Number(date[1]) - 1, Number(date[0]))
      })
    ).subscribe(operador => this.operador = operador)

  }

  guardar() {

    if (!this.isValidForm()) {
      return;
    }
    this.operador.fechaNacimiento = this.formatDate();

    if (this.operador.id) {
      this.operariosService.actualizarOperario(this.operador)
        .subscribe(operario => {
          this.router.navigate(['/operarios/listado'])
          this.mostrarSnackBar('Registro actualizado')
        })
    } else {
      this.operariosService.agregarOperario(this.operador)
        .subscribe(operario => {
          this.router.navigate(['/operarios/editar/', operario.id])
          this.mostrarSnackBar('Registro creado')
        })
    }

  }

  isValidForm() {
    if (this.operador.apellido.trim().length === 0) {
      return false;
    }

    if (this.operador.nombre.trim().length === 0) {
      return false;
    }

    if (this.operador.dni.trim().length === 0) {
      return false;
    }

    if (!this.fecha) {
      return false;
    }
    if (!this.operador.turno) {
      return false;
    }
    return true;
  }

  formatDate(): string {
    let day = '' + (this.fecha.getDay() - 1)
    let month = '' + (this.fecha.getMonth() + 1)
    const year = '' + this.fecha.getFullYear()

    if (month.length < 2) {
      month = '0' + month;
    }

    if (day.length < 2) {
      day = '0' + day;
    }

    return [day, month, year].join('/');
  }

  mostrarSnackBar(mensaje: string) {
    this.snackBar.open(mensaje, 'ok!', {
      duration: 2500
    })
  }

}


