export interface Operario {
  id?: number;
  nombre: string;
  apellido: string;
  dni: string;
  img?: string;
  estado: boolean;
  direccion?: string;
  fechaNacimiento: string;
  turno: string;
}
